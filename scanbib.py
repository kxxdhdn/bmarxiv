import numpy as np
from tqdm import tqdm
import random
from bmarxiv import scan

# import re


def scanbib(fname, kws):

    samplesize = 1000

    with open(fname, "r") as f:
        bib = f.readlines()

    bib = [b for b in bib if "abstract =" in b or "title =" in b]
    bib = np.array(bib)[random.sample(sorted(np.arange(len(bib))), samplesize)]
    bib = (
        ". ".join(bib)
        .replace("\n", "")
        .replace("\\", "")
        .replace("<BR>", "")
        .replace("<br>", "")
        .replace("<BR", "")
        .replace("<br", "")
        .replace("{", "")
        .replace("}", "")
        .replace("abstract =", "")
        .replace("title =", "")
    )

    res = {}

    print("Scanning for keywords with no match...")
    matches, kwok = scan(bib, kws)
    nomatch = set(kws) - set(kwok)
    res.update({"nomatch": nomatch})

    toremove = "'`.,;!?[]"
    for r in toremove:
        bib = bib.replace(r, "")
    tmp = np.array(bib.split())

    with open("ignore.dat") as f:
        toignore = f.readlines()
    toignore = sorted(toignore)
    with open("ignore.dat", "w") as f:
        f.writelines(toignore)
    toignore = [t.replace("\n", "") for t in toignore]

    # 1 word
    print("=================")
    minocc = 0.05 * samplesize
    print("Checking single words")
    tmp = np.array(
        [t for t in tmp if t.lower() not in toignore and t.strip() != "" and len(t) > 2]
    )
    allwords = list(set(tmp))
    counts = {w: np.sum(tmp == w) for w in tqdm(allwords)}
    sd = dict(sorted(counts.items(), key=lambda x: x[1])[::-1])
    sd = {s: sd[s] for s in sd if sd[s] > minocc}
    print(sd)
    res.update({1: sd})

    # 2 words
    print("=================")
    print("Checking groups of 2 words")
    minocc = 0.01 * samplesize
    tmp2 = np.array([" ".join(tmp[i : i + 2]) for i in range(len(tmp) - 2)])
    tmp2 = np.array(
        [
            t
            for t in tmp2
            if t.lower() not in toignore and t.strip() != "" and len(t) > 2
        ]
    )
    allwords = list(set(tmp2))
    counts = {w: np.sum(tmp2 == w) for w in tqdm(allwords)}
    sd = dict(sorted(counts.items(), key=lambda x: x[1])[::-1])
    sd = {s: sd[s] for s in sd if sd[s] > minocc}
    print(sd)
    res.update({2: sd})

    # 3 words
    print("=================")
    print("Checking groups of 3 words")
    minocc = 0.005 * samplesize
    tmp2 = np.array([" ".join(tmp[i : i + 3]) for i in range(len(tmp) - 3)])
    tmp2 = np.array(
        [
            t
            for t in tmp2
            if t.lower() not in toignore and t.strip() != "" and len(t) > 2
        ]
    )
    allwords = list(set(tmp2))
    counts = {w: np.sum(tmp2 == w) for w in tqdm(allwords)}
    sd = dict(sorted(counts.items(), key=lambda x: x[1])[::-1])
    sd = {s: sd[s] for s in sd if sd[s] > minocc}
    print(sd)
    res.update({3: sd})

    return res
