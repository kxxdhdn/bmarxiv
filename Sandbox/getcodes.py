# -----------------------------------------------------------------
with open("/local/home/vleboute/ownCloud/Config/tokens/ads.token", "r") as f:
    adstoken = f.readlines()
    adstoken = adstoken[0].replace("\n", "").strip()

# -----------------------------------------------------------------

import requests

q = 'bibstem:"ascl.soft"'

r = requests.get(
    "https://api.adsabs.harvard.edu/v1/search/query",
    params={
        "q": f"{q}",
        "start": "0",
        "rows": "10",
        "sort": "date desc",
        # Allowed: id asc ┃ author_count asc ┃ bibcode asc ┃ citation_count asc ┃ citation_count_norm asc ┃ classic_factor asc ┃ first_author asc ┃ date asc ┃ entry_date asc ┃ read_count asc' ┃ score asc ┃ id desc ┃ author_count desc ┃ bibcode desc ┃ citation_count desc ┃ citation_count_norm desc ┃ classic_factor desc ┃ first_author desc ┃ date desc ┃ entry_date desc ┃ read_count desc ┃ score desc
        "wt": "json",
        "fl": ["title", "date", "entry_date"]
        # Allowed: abstract ┃ ack ┃ aff ┃ aff_id ┃ alternate_bibcode ┃ alternate_title ┃ arxiv_class ┃ author ┃ author_count ┃ author_norm ┃ bibcode ┃ bibgroup ┃ bibstem ┃ citation ┃ citation_count ┃ cite_read_boost ┃ classic_factor ┃ comment ┃ copyright ┃ data ┃ database ┃ date ┃ doctype ┃ doi ┃ eid ┃ entdate ┃ entry_date ┃ esources ┃ facility ┃ first_author ┃ first_author_norm ┃ grant ┃ grant_agencies ┃ grant_id ┃ id ┃ identifier ┃ indexstamp ┃ inst ┃ isbn ┃ issn ┃ issue ┃ keyword ┃ keyword_norm ┃ keyword_schema ┃ lang ┃ links_data ┃ nedid ┃ nedtype ┃ orcid_pub ┃ orcid_other ┃ orcid_user ┃ page ┃ page_count ┃ page_range ┃ property ┃ pub ┃ pub_raw ┃ pubdate ┃ pubnote ┃ read_count ┃ reference ┃ simbid ┃ title ┃ vizier ┃ volume ┃ year
    },
    headers={"Authorization": f"Bearer:{adstoken}"},
)


print(r.content.decode())
# https://ui.adsabs.harvard.edu/help/api/api-docs.html
# https://ui.adsabs.harvard.edu/help/search/search-syntax
